"""
@FileName：conftest.py
@Description：
@Author：Mark
@Time：5/12/23 1:06 PM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""
import pytest
from service_base.service_base import ServiceBase




def service():
    """Create and configure a new service instance for each test"""

    # Set testing environment variables
    # monkeypatch.setenv('FLASK_ENV', 'production')  # Force production config
    # monkeypatch.setenv('FORCE_IN_MEM_DB', 'True')  # Force in-memory database. Env var must be a str
    #
    # # Create temp database and mock service config for each class
    # database_path = tmp_path_factory.mktemp("database").joinpath('testing_db.json').as_posix()  # TODO: Migrate to Mongo
    # service_config_path = mock_service_config
    #
    # # Create service instance
    print('start')
    instance = create_service_instance( {'TESTING': True, 'DATABASE_URI': ''})
    assert instance.DEBUG is False
    assert instance.DEBUG is not None
    instance.app_database.devices.delete_many({})
    instance.app_database.device_templates.delete_many({})
    yield instance
    instance.app_database.devices.delete_many({})
    instance.app_database.device_templates.delete_many({})


def create_service_instance(config_file, test_config=None):
    # Create Flask instance and set config file path
    # - CI environments can use test_config dict to set/overwrite Flask configuration early enough
    service_instance = Service(__name__, config_file, test_config)

    return service_instance

class Service(ServiceBase):
    """Service object"""

    def __init__(self, import_name, config_path, test_config=None):

        # Create Flask instance
        super().__init__(import_name, config_path, test_config)


if __name__== '__main__':
    print('aaa')
    service()