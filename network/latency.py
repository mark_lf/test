"""
@FileName：latency.py
@Description：test network latency. 100 ping
@Author：Mark
@Time：4/14/23 4:26 PM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""

import os, re
import numpy as np

PINGCMD = 'ping -D -c 86400 192.168.0.16 > test.log'
os.system(PINGCMD)
latency = []
with open('test.log', 'r') as f:
    while (True):
        line = f.readline()
        if not line:
            break
        result = re.search('time=', line)
        if result:
            latency.append(float(line[result.end():-4]))

print('max:', round(np.max(latency), 2), '\n min:', round(np.min(latency), 2), '\n  mean:', round(np.mean(latency), 2),
      '\n std:', round(np.std(latency), 2))
