"""
@FileName：test1.py
@Description：
@Author：Mark
@Time：4/20/23 11:13 AM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""
import gevent
from gevent.pool import Pool
from random import random
from time import sleep
CONCURRENT = 4
import os
import time
from threading import Thread,current_thread
def task1():
    for i in range(5):
        print('{}洗衣服:'.format(current_thread().name), i, current_thread().ident,os.getpid(), os.getppid())
        time.sleep(0.5)
def task2(n):
    for i in range(n):
        print('{}劳动最光荣，扫地中...'.format(current_thread().name), i, current_thread().ident,os.getpid(), os.getppid())
        time.sleep(0.5)
def thread_test(no:int):
    print('main:', os.getpid())
    job_list= []
    # 创建线程对象
    t1 = Thread(target=task1,name='警察'+str(no))
    t2 = Thread(target=task2,name='小偷'+str(no), args=(6,))
    job_list.append(t1)
    job_list.append(t2)
    # 启动线程
    t1.start()
    t2.start()
def testpool(npool, count):
    print('')
    print('threadpool %d started.' % count)
    ran = int(random() * 20)+1
    print('random is ', ran)
    for i in range(1, ran):
        npool.spawn(thread_test(i))
    print('length when spawn', len(npool))
    print('threadpool %d joined.' % count)
    npool.join()
    print('threadpool %d done.' % count)

counter = 0
pool = Pool(CONCURRENT)
while 1:
    counter += 1
    # start to testpool
    testpool(pool, counter)
    print('length in the loop  ', len(pool))
    # sleep(10)
    # # sleep
    # sleep(0.3)

    #await
    if counter ==3:
        break