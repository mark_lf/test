#/bin/sh
pip install poetry
poetry -V


# for ohmyzsh
#mkdir -p $ZSH_CUSTOM/plugins/poetry
#poetry completions zsh > $ZSH_CUSTOM/plugins/poetry/_poetry

rm -rf .venv
#set local venv
poetry config virtualenvs.in-project true
poetry config virtualenvs.path  .venv
poetry init -n --dev-dependency=urllib3_mock@latest --dev-dependency=requests_mock@latest  --dev-dependency=pytest-cov@latest --dev-dependency=pytest-randomly@latest --dev-dependency=semver="<3" --dev-dependency=python-gitlab="^3.14.0" --dev-dependency=python-semantic-release="^7.33.3" --dev-dependency=flake8@latest --dev-dependency=pytest@latest --dev-dependency=mypy@latest
poetry add apispec@latest apispec-webframeworks@latest

cat <<EOF > poetry.toml
[http-basic.gitlab]
username = "packagetoken"
password = "glpat-mLVdeaHwyzsvSdk4tU1q"
EOF

poetry source add --secondary gitlab "https://gitlab.com/api/v4/groups/8077510/-/packages/pypi/simple"
#poetry add --source gitlab private-package
poetry config certificates.gitlab.cert false
for item in `pipenv requirements --exclude-markers |sed 's/==.*//g' |awk 'NR>2 {print }'|awk 'BEGIN{ORS=" "} {print}' `
do
  echo $item
  poetry add -q $item
done
#poetry install -q


# Update Dockerfile
sed -i.bak -e '2,300s/build\s+/poetrybuild /g' -e 's/pipenv sync/poetry install/' -e 's/PIPENV_VENV_IN_PROJECT/POETRY_VIRTUALENVS_IN_PROJECT/' -e 's/pipenv/poetry/g' -e 's/COPY Pipfile \./COPY pyproject\.toml \.\nCOPY poetry\.toml \./' -e 's/Pipfile/poetry/' Dockerfile
rm Dockerfile.bak


# Update .envrc
for i in `seq 3`
do
  sed -i.bak -e '$d' .envrc
done
rm .envrc.bak
cat <<EOF > .envrc
# This file will automatically be loaded before the first prompt
export PYTHONSTARTUP=interactive_dev.py
# Activate Poetry
layout_poetry  # Load Python virtual environment using Poetry
EOF


#update gitlab_ci
sed -i.bak -e '/CI Dep Update Jobs/,+15d' -e '/PIPENV_VENV_IN_PROJECT/d' -e '/dep-update/d' -e 's/pipenv sync --dev/poetry install/' -e 's/messasge/message/' -e 's/- generate-lock//' -e 's/pipenv:0.2.0-git/poetry-git-buildctl:0.1.0/g' -e 's/pipenv:0.2.0/poetry-git-buildctl:0.1.0/g' -e 's/pipenv/poetry/g' .gitlab-ci.yml
rm .gitlab-ci.yml.bak


sed -i.bak -e '/Pipfile.lock/i\
!poetry.lock' -e '/Pipfile.lock/i\
!poetry.toml'  -e '/Pipfile.lock/i\
!pyproject.toml'  -e '/Pipfile/d' .dockerignore
rm .dockerignore.bak
sleep 60
rm Pipfile
rm Pipfile.lock


# manual update
# python = "~3.10"