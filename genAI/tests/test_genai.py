"""
@FileName：test_genai.py
@Description：
@Author：Mark
@Time：5/11/23 3:25 PM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""
from genai import __version__
def test_version():
    assert __version__ =="0.1.0"