"""
@FileName：test1.py
@Description：
@Author：Mark
@Time：4/27/23 1:29 PM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""

# import the necessary packages
import numpy as np
import argparse
import cv2

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
ap.add_argument("-m", "--margin-safe", required=False, default=50, help="Safe margin in pixels, default is 10")
ap.add_argument("-s", "--shape", required=False, default='cir',
                help="Shape, default is cir, short for circle. Alternative is rec, short for rectangle")
args = vars(ap.parse_args())
print(args)
# load the image, clone it for output, and then convert it to grayscale
image = cv2.imread(args["image"])
output = image.copy()
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

circles = cv2.HoughCircles(gray, cv2.HOUGH_GRADIENT, 1.2, 1000)


# def mask_img(img):
#     h, w, _ = img.shape
#     mask = np.zeros((h, w), np.uint8)
#     cv2.circle(mask, (678, 321), 5, 255, 654)
#     img3 = cv2.bitwise_and(img, img, mask=mask)
#

# ensure at least some circles were found
if circles is not None:
    if len(circles) == 1:
        # convert the (x, y) coordinates and radius of the circles to integers
        circles = np.round(circles[0, :]).astype("int")
        # loop over the (x, y) coordinates and radius of the circles
        for (x, y, r) in circles:
            # draw the circle in the output image, then draw a rectangle
            # corresponding to the center of the circle
            cv2.circle(output, (x, y), r - int(args["safe_margin"]), (0, 255, 0), 4)
            h, w, _ = image.shape
            mask = np.zeros((h, w), np.uint8)
            cv2.circle(mask, (x, y), r - int(args["safe_margin"]), 255, -1)
            output = cv2.bitwise_and(output, output, mask=mask)
        # show the output image
        # cv2.imshow("output", np.hstack([image, output]))
        cv2.imshow("output", output)
        cv2.imwrite("masked.jpg", output)
        cv2.waitKey(0)
    else:
        raise Exception('more than 1 circle is detected, check the parameters')
