"""
@FileName：mask.py
@Description：
@Author：Mark
@Time：4/27/23 2:05 PM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""
import cv2
import numpy as np
img = cv2.imread('dog.jpeg')


h,w,_ = img.shape
mask = np.zeros((h,w), np.uint8)

cv2.circle(mask, (678,321), 5, 255, 654)

cv2.imshow('img',img)


# img = cv2.bitwise_and(img, mask)
# cv2.imshow('resultImg1', img)
