"""
@FileName：app.py.py
@Description：
@Author：Mark
@Time：4/19/23 3:16 PM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""
#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask import Flask
import requests


app = Flask(__name__)


@app.route("/test")
def test():
    requests.get("https://www.baidu.com")
    return "test"


if __name__ == "__main__":
    app.run(debug=True)