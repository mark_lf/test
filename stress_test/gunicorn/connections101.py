"""
@FileName：connections100.py
@Description：
@Author：Mark
@Time：4/18/23 1:40 PM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""

from locust import HttpUser, task, constant


class FlexivSleep(HttpUser):
    wait_time = constant(0.5)
    host = "http://hz-onsite.life-foundry.com"

    @task
    def test_get_method(self):
        self.client.get("/api/v1/dev-man/devices/mock-flexiv/call/sleep", headers=self.headers, timeout=30)
    # @task
    # def test_get_method2(self):
    #     self.client.get("/api/v1/dev-man/devices/agv/call/position", headers=self.headers, timeout=30)
    def on_start(self):
        tokenResponse = self.client.post("/api/v1/auth/login",
                                         json={"username": "lifefoundry", "password": "Foundry4Life"})
        response = tokenResponse.json()
        responseToken = response['token']
        self.headers = {'Authorization': 'Bearer ' + responseToken}
        print(self.headers)

# class AGVPosition(HttpUser):
#     wait_time = constant(1)
#     host = "http://hz-onsite.life-foundry.com"
#     @task
#     def test_get_method(self):
#         self.client.get("/api/v1/dev-man/devices/mock-flexiv/call/position", headers=self.headers, timeout=30)
#
#     def on_start(self):
#         tokenResponse = self.client.post("/api/v1/auth/login",
#                                          json={"username": "lifefoundry", "password": "Foundry4Life"})
#         response = tokenResponse.json()
#         responseToken = response['token']
#         self.headers = {'Authorization': 'Bearer ' + responseToken}
#         print(self.headers)


# class AzureFlexivSleep(HttpUser):
#     wait_time = constant(0)
#     host = "https://azure-testing.life-foundry.com"
#
#     @task
#     def test_get_method(self):
#         self.client.get("/api/v1/transport/devices/mock-flexiv/sleep", headers=self.headers, timeout =30)
# #/api/v1/transport/devices/mock-flexiv/sleep
#     def on_start(self):
#         tokenResponse = self.client.post("/api/v1/auth/login", json={"username": "root", "password": "root"})
#         response = tokenResponse.json()
#         responseToken = response['token']
#         self.headers = {'Authorization': 'Bearer ' + responseToken}
#         print(self.headers)


# class LocalFlexivPosition(HttpUser):
#     wait_time = constant(1)
#     host = "http://192.168.0.16:31520"
#
#     @task
#     def test_get_method(self):
#         result = self.client.get("/devices/mock-flexiv/call/position")
#         print(result)
#/api/v1/transport/devices/mock-flexiv/sleep http://192.168.0.16:31520/devices/mock-flexiv/call/position"

# class LocalFlexivPosition(HttpUser):
#     wait_time = constant(1)
#     host = "http://www.bing.com"
#
#     @task
#     def test_get_method(self):
#         self.client.get("/dict")