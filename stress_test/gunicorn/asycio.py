"""
@FileName：asycio.py
@Description：
@Author：Mark
@Time：4/19/23 11:40 AM
@Department：Software Engineering
@Website：www.life-foundry.com
@Copyright：©2019-2023 Life-Foundry LTD.
"""
import aiohttp
import asyncio, time


async def main_1request():
    async with aiohttp.ClientSession() as session:
        pokemon_url = 'http://hz-onsite.life-foundry.com/api/v1/dev-man/devices/mock-flexiv/call/sleep'
        async with session.get(pokemon_url, headers={
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJsaWZlZm91bmRyeSIsInBlcm1pc3Npb25zIjpbImFkbWluIl19.4XZay21FWTenYrsVE6Y4txZcCVMF2VBbFMfTFJIp8YE'}
                               ) as resp:
            result = await resp.json()
            print(result)


async def run_150requests():
    async with aiohttp.ClientSession() as session:
        for number in range(1, 11):
            pokemon_url = 'http://hz-onsite.life-foundry.com/api/v1/dev-man/devices/mock-flexiv/call/sleep'
            async with session.get(pokemon_url, headers={
                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJsaWZlZm91bmRyeSIsInBlcm1pc3Npb25zIjpbImFkbWluIl19.4XZay21FWTenYrsVE6Y4txZcCVMF2VBbFMfTFJIp8YE'}
                                   ) as resp:
                result = await resp.json()
                print(result)


async def get_pokemon(session, url):
    async with session.get(url, headers={
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJsaWZlZm91bmRyeSIsInBlcm1pc3Npb25zIjpbImFkbWluIl19.4XZay21FWTenYrsVE6Y4txZcCVMF2VBbFMfTFJIp8YE'}
                           ) as resp:
        result = await resp.json()
        return result


async def gather_run():
    async with aiohttp.ClientSession() as session:

        tasks = []
        for number in range(1, 101):
            url = 'http://hz-onsite.life-foundry.com/api/v1/dev-man/devices/mock-flexiv/call/sleep'
            tasks.append(asyncio.ensure_future(get_pokemon(session, url)))

        original_pokemon = await asyncio.gather(*tasks)
        for pokemon in original_pokemon:
            print(pokemon)


if __name__ == '__main__':
    for i in range(10):
        start_time = time.time()
        asyncio.run(gather_run())
        print("--- %s seconds ---" % (time.time() - start_time))
